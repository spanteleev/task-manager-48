package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.task.TaskGetByIdRequest;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDTO task = getTaskEndpoint().showTaskById(new TaskGetByIdRequest(getToken(), id)).getTask();
        showTask(task);
    }

}
