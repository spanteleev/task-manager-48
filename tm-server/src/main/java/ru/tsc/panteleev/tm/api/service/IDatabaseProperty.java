package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUserName();

    @NotNull
    String getDatabaseUserPassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseUseSecondLvlCache();

    @NotNull
    String getDatabaseCacheConfigFile();

    @NotNull
    String getDatabaseCacheFactoryClass();

    @NotNull
    String getDatabaseUseQueryCache();

    @NotNull
    String getDatabaseUseMinimalPuts();

    @NotNull
    String getDatabaseCacheRegionPrefix();



}

